This package contains the nonfree data for Boxes. Unlike rest of the Boxes
contents, product logos are not licensed under LGPLv2+ and hence the need to
ship it in a separate pacakge. For more details on the rationale for the
existance package/repository:

https://bugzilla.gnome.org/show_bug.cgi?id=671251

Boxes has acquired explicit permission from trademark owners for the usage and
shipment of their logos[1][3]. Please not that this permission does not extend
to any derivate work but only applies to Boxes for the very specific purpose of
identifying the products in question.

Following are the owners for each logo:

Fedora: Red Hat, Inc.
Ubuntu: Canonical Ltd.
openSUSE: Novell, Inc. [2]
Debian: SPI (Software in the Public Interest).

The Fedora board/legal required we keep a special note in this file:

   Fedora®, the Fedora word design, and the Infinity design logo, either
   separately or in combination, are hereinafter referred to as "Fedora
   Trademarks" and are trademarks of Red Hat, Inc., registered in the
   United States and other countries. The Boxes software contains images
   that are or include the Fedora Trademarks. You are granted a a
   license to copy and redistribute unmodified image files containing
   the Fedora Trademarks only for the purpose of indicating Fedora VM
   instances in the Graphical Interface for the Boxes software, as
   described to the Fedora Board on 2012-02-21. No other permission is
   granted.

   All such rights are granted to you provided that the above trademark
   legend and this license are included with each copy you make, and
   they remain intact and are not altered, deleted, or modified in any
   way.
-----------------------------------------------------------------------

[1] with the exception of Debian logo due to its very liberal licensing:

http://www.debian.org/logos/

[2] For use in other projects, please read:

http://en.opensuse.org/openSUSE:Trademark_guidelines

[3] Following are the e-mails I recieved from vendors granting us explicit
    permissions to use their logos in Boxes:

Fedora
======

http://lists.fedoraproject.org/pipermail/advisory-board/2012-February/011367.html

openSUSE
========

From: awafaa@opensuse.org Andrew Wafaa
Date: Wed, 22 Feb 2012 12:16:08 +0200
Subject: Re: Permission to use openSUSE Logo for gnome-boxes

Hi Manu,

On 21 February 2012 17:48, Manu Gupta <manugupt1@gmail.com> wrote:
> Hi,
>  As you may or may not know, we have been working on a new GNOME
> application, Boxes[1] that (among other things) enables users to easily
> manage virtual-machines. This also includes ability to detect
> operating-systems given an installation media and presenting an
> intuitive and *impressive* UI to user to easily install different
> operating systems with minimum or no interaction. Boxes makes use of a
> new library, libosinfo[2] that provides information about operating
> systems (which it internally keeps in an XML database).
>
> We really want (more like need) to show logos of different operating
> systems to the user[3]. Can we please get the permission to use openSUSE logo in
> this manner and distribute it along with Boxes or libosinfo?
>
> [1] http://live.gnome.org/Boxes
> [2] https://fedorahosted.org/libosinfo/
> [3] http://static.fi/~zeenix/tmp/boxes-small-iso-icons+logos.png
>
>
> --
> Regards
> Manu Gupta
>

openSUSE would like to have our logo show up as one of the installable
OSes within Boxes. Please ensure that you follow the Trademark
Guidlines[0]. If you would like to modify the logo to work better with
the application, please submit your proposal of a modified logo for
approval. This is *ONLY* for openSUSE and _NOT_ for SUSE Enterprise
Linux.

Many thanks and regards,

Andy

0= http://en.opensuse.org/openSUSE:Trademark_guidelines

--
Andrew Wafaa
IRC: FunkyPenguin
GPG: 0x3A36312F
=================

From: mrdocs@opensuse.org Peter Linnell
Date: Wed, 22 Feb 2012 20:23:31 +0200
Subject: Re: Permission to use openSUSE Logo for gnome-boxes

On 02/22/2012 02:16 AM, Andrew Wafaa wrote:
> Hi Manu,
>
> On 21 February 2012 17:48, Manu Gupta<manugupt1@gmail.com>  wrote:
>> Hi,
>>   As you may or may not know, we have been working on a new GNOME
>> application, Boxes[1] that (among other things) enables users to easily
>> manage virtual-machines. This also includes ability to detect
>> operating-systems given an installation media and presenting an
>> intuitive and *impressive* UI to user to easily install different
>> operating systems with minimum or no interaction. Boxes makes use of a
>> new library, libosinfo[2] that provides information about operating
>> systems (which it internally keeps in an XML database).
>>
>> We really want (more like need) to show logos of different operating
>> systems to the user[3]. Can we please get the permission to use openSUSE logo in
>> this manner and distribute it along with Boxes or libosinfo?
>>
>> [1] http://live.gnome.org/Boxes
>> [2] https://fedorahosted.org/libosinfo/
>> [3] http://static.fi/~zeenix/tmp/boxes-small-iso-icons+logos.png
>>
>>
>> --
>> Regards
>> Manu Gupta
>>
>
> openSUSE would like to have our logo show up as one of the installable
> OSes within Boxes. Please ensure that you follow the Trademark
> Guidlines[0]. If you would like to modify the logo to work better with
> the application, please submit your proposal of a modified logo for
> approval. This is *ONLY* for openSUSE and _NOT_ for SUSE Enterprise
> Linux.
>
> Many thanks and regards,
>
> Andy
>
> 0= http://en.opensuse.org/openSUSE:Trademark_guidelines
>

Hi Manu,

I am with Andy. Good luck.

Cheers,
Peter
=====

Ubuntu
======

From: michelle@canonical.com michelle@canonical.com
Date: Tue, 21 Feb 2012 17:46:02 +0200
Subject: Your application to Ubuntu to Trademarks

Hi Zeeshan,

Thank you for contacting us and for your interest in and support of Ubuntu.

============

We have been working on a new GNOME
application, Boxes[1] that (among other things) enables users to easily
manage virtuat-machines. This also includes ability to detect
operating-systems given an installation media and presenting an
intuitive and impressive UI to user to easily install different
operating systems with minimum or no interaction. Boxes makes use of a
new library, libosinfo[2] that provides information about operating
systems (which it internally keeps in an XML database).

Can we get explicit permission to use the Ubuntu logo (as shown in the screen[3]) and distribut it as part of libosinfo or Boxes itself?

[1] https://live.gnome.org/Boxes
[2] https://fedorahosted.org/libosinfo/
[3] http://static.fi/~zeenix/tmp/boxes-small-iso-icons+logos.png

============

Canonical is the official sponsor of the Ubuntu project and owns the trademarks, both in the word and logo form, relating to the project.

To keep the balance between the integrity of our trademarks and the ability to customise Ubuntu, we’ve tried to define a reasonable trademark policy that allows our Community to use Ubuntu and to promote the project. You can read the full policy at http://www.ubuntu.com/aboutus/trademarkpolicy.

As your intent is to promote the Ubuntu brand and show your support of the software, we have no issue with you producing the product as you have requested.

You can find our brand guidelines and downloadable images at http://design.ubuntu.com/.

Thanks again for your support.

Kind regards,


Michelle,
Trademarks Team

Michelle Surtees-Myers
Ubuntu - Linux for Human Beings
